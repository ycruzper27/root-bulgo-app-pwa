import Vue from 'vue'
import VueRouter from 'vue-router'
import { routesLoginBulgoApp } from 'login-bulgo-app';
import { routesTableBulgoApp } from 'table-bulgo-app';
Vue.use(VueRouter)

const routes = [
  ...routesLoginBulgoApp,
  ...routesTableBulgoApp,
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
