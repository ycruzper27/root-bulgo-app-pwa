import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

// * Notification Component
import Notifications from 'vue-notification'

// * Styles from projects
import 'login-bulgo-app/dist/login-bulgo-app.css';
import 'table-bulgo-app/dist/table-bulgo-app.css';
import 'bulgo-spa-library/dist/bulgo-spa-library.css';

Vue.config.productionTip = false

Vue.use(Notifications)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app')
