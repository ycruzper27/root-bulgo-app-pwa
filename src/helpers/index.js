import { storeBulgo, registerFiltersAndHelpersVue, ModeApp } from 'bulgo-spa-library'
import Vuetify from '../plugins/vuetify';

export default class Helper extends ModeApp {

    constructor(props) {
        super(props);
        registerFiltersAndHelpersVue(Vuetify.framework, true, process.env);
        if(!/localhost/.test(window.location.host))
            this.registerOneSignalApp();
    }

    registerOneSignalApp() {
        window.OneSignal = window.OneSignal || [];
            window.OneSignal.push(async () => {
            window.OneSignal.init({
                appId: "8ca64768-ae7f-42ee-9ac4-b59e25592a2d",
                safari_web_id: "web.onesignal.auto.5f257b48-e003-493b-a584-79b35812c24b",
                allowLocalhostAsSecureOrigin: process.env.NODE_ENV !== 'production',
            });
            await window.OneSignal.showNativePrompt();
            console.log('show prompt and register player id!', window.OneSignal);
            await this.registerPlayerPush();
        });
    }

    async registerPlayerPush () {

        let userId = null
        console.log("Try register push", window.OneSignal)
        if(typeof window.OneSignal === 'function' || window.OneSignal.VERSION) {
            // enable async function
            try {
                userId = await window.OneSignal.getUserId()
                console.log('userID try and try dispatch vuex PLAYER ---->', userId, storeBulgo)
                storeBulgo.dispatch('savePlayerIdOneSignal', userId);
                return userId
            } catch (e) {
                userId = null
                console.log('userID catch', userId, e)
                return e
            }
        } return userId
    }
}