import Vue from 'vue'
import Vuex from 'vuex'
import { TableAppModule, TableAppGetters } from 'table-bulgo-app';

// const modulesFiles = require.context('./modules', true, /\.js$/)

// const modules = modulesFiles.keys().reduce((modules, modulePath) => {
//   const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
//   const value = modulesFiles(modulePath)
//   modules[moduleName] = value.default
//   return modules
// }, {})

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    [TableAppModule.name]: TableAppModule,
  },
  getters: {
    ...TableAppGetters
  }
})
