# root-bulgo-app-pwa

## Project setup
```
npm install
```
## Update PWA And publish

1. Install library module `npm i ${module}-bulgo-app`
2. **main.js** import styles file from module bulgo:
    ```javascript
        import '${module}-bulgo-app/dist/${module}-bulgo-app.css';
    ```
3. **index.js** in `/router`folder, import routes:
    ```javascript
        import module${name}AppBulgo from '{module}-bulgo-app';
    ```

4. Add routes in routes locale:
    ```javascript
        ...module${name}AppBulgo.routes${name}BulgoApp,
    ```
5. **Update** command `npm run bulgo:update` whit this: ` && npm i ${module}-bulgo-app@latest`