module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  css: {
    loaderOptions: {
        scss: {
          additionalData: `
            @import "@/assets/scss/global.scss";
          `
        }
    }
  },
  pwa: {
    workboxOptions: {
      exclude: [
        /OneSignal.*\.js$/,
        /\.map$/, /_redirects/
      ],
    },
  },
}
